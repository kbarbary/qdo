"""
Lightweight infrastructure to process many little tasks in a queue.

QDO (kew-doo) is a toolkit for managing many many small tasks
within a larger batch framework.  QDO separates the queue of tasks
to perform from the batch jobs that actually perform the tasks.
This simplifies managing tasks as a group, and provides greater
flexibility for scaling batch worker jobs up and down or adding additional
tasks to the queue even after workers have started processing them.

The qdo module provides an API for interacting with task queues.
The qdo script uses this same API to provide a command line interface
that can be used interchangeably with the python API.
Run "qdo -h" to see the task line options.

Example:

#- Load queue
import qdo
q = qdo.create('EchoChamber')
for i in range(100):
    q.add('echo '+str(i))

#- Launch 10 batch jobs to process these tasks
q.launch(10)

#- The worker jobs are basically doing this:
import qdo
q = qdo.connect('EchoChamber')
q.do()

Stephen Bailey, Fall 2013
"""

__version__ = "0.5"

import sys
import os
import subprocess
import time
import json   #- For encoding/decoding objects as strings
import uuid   #- For unique message IDs
import shlex
from ConfigParser import SafeConfigParser

#-------------------------------------------------------------------------
#- Lightweight wrapper class for tasks
        
class Task(object):
    """
    Wrapper class for a single qdo task
    """
    #- Enumeration of various states a Task could be in
    ### BLOCKED   = 'Blocked'       #- A required dependency failed
    WAITING   = 'Waiting'       #- Waiting for dependencies to finish
    PENDING   = 'Pending'       #- Eligible to be selected and run
    RUNNING   = 'Running'       #- Running right now
    SUCCEEDED = 'Succeeded'     #- Finished with err code = 0
    FAILED    = 'Failed'        #- Finished with err code != 0
    UNKNOWN   = 'Unknown'       #- Something went wrong and we lost track
    VALID_STATES = [WAITING, PENDING, RUNNING, SUCCEEDED, FAILED]
    
    def __init__(self, task, queue, id=None, state=UNKNOWN, err=None, message=None, **kwargs):
        """
        Create Task object
        
        Required:
        task    : input string or JSON-able object
        Either `queue` or `queuename`:
            queue     : parent Queue that this task is associated with
            queuename : name of queue to use
        
        Optional:
        id      : unique id string to identify this task;
                  default will use uuid4() to generate unique id.
        state   : one of the states in Task.VALID_STATES or Task.UNKNOWN
        err     : integer error code
        message : message string
        
        kwargs are ignored, allowing Tasks to be created from dictionaries
        with more keys than strictly necessary
        """
        if id is None:
            self.id = str(uuid.uuid4())
        else:
            self.id = id
            
        self.task = task
        if isinstance(queue, Queue):
            self.queue = queue
        else:
            self.queue = connect(queue)
            
        self.state = state
        self.err = err
        self.message = message

    #- Implement iterator to allow conversion x = dict(task)
    #- Convert back to Task with t = Task(**x)
    def __iter__(self):
        yield 'task', self.task
        yield 'id', self.id
        yield 'state', self.state
        yield 'err', self.err
        yield 'message', self.message
        yield 'queuename', self.queue.name

    def __repr__(self):
        return json.dumps(dict(self))

    def run(self, quiet=False, script=None, func=None):
        """
        Treat task as a command to run via subprocess.call()       

        If *script* is given, it is prefixed to the task or used as a
        template string to format tasks that are lists or dictionaries.
        
        If *func* is given, call func(task) instead of spawning a command.

        Return resulting error code (0=success)
        """
        if not quiet:
            print "## -----------------------------------------------------------------------"
            # print "## QDO Starting %s  %s (%s)" % (self.id, time.asctime(), time.time())
            print "## QDO Starting %s  %s" % (self.id, time.asctime())
            print "## QDO Task:", self.task
            if script is not None:
                print "## QDO Task script:", script

        sys.stdout.flush()
        sys.stderr.flush()
        
        #- Be optimistic
        err = 0
        message = None

        #- Should we pass the task to a python function?
        if func is not None:
            try:
                result = func(self.task)
                if result is not None:
                    message = str(result)
            except Exception, e:
                print e
                message = e.message
                err = 42
                
        #- Otherwise try spawning a subprocess.call()
        else:
            try:
                cmd = self.task
                if script is not None:
                    try:
                        if isinstance(cmd, (str, unicode)):
                            cmd = script + ' ' + cmd
                        elif isinstance(cmd, dict):
                            cmd = script.format(**cmd)
                        elif isinstance(cmd, (list, tuple)):
                            cmd = script.format(*cmd)
                    except:
                        print >> sys.stderr, "ERROR: failed to combine script with args"
                        err = 1

                #- No parsing failures, so let's try to run it
                if err == 0:
                    if isinstance(cmd, (str, unicode)):
                        err = subprocess.call(cmd, shell=True)
                    else:
                        print >> sys.stderr, "ERROR: task is not a string or args to a script"
                        err = 126
                
            except OSError, oserr:
                err = oserr.errno
        
        #- Wrap up the state of what just happened
        sys.stdout.flush()
        sys.stderr.flush()
        if err > 0:
            self.set_state(Task.FAILED, err=err, message=message)
            if not quiet:
                # print "## QDO FAILED   %s  %s (%s)" % (self.id, time.asctime(), time.time())
                print "## QDO FAILED   %s  %s" % (self.id, time.asctime())
                print "## QDO Error Code", err
        else:
            self.set_state(Task.SUCCEEDED, err=err, message=message)
            if not quiet:
                # print "## QDO SUCCESS  %s  %s" % (self.id, time.asctime(), time.time())
                print "## QDO SUCCESS  %s  %s" % (self.id, time.asctime())

        if not quiet:
            print
            
        return err
          
    def set_state(self, state, err=None, message=None):
        """
        Set the state of this task.
        
        `state` can be any string, but generally should be one of the values 
        encoded with Task.VALID_STATES.
        
        Optionally provide integer err code and/or message string.
        """
        self.state = state
        self.err = err
        self.message = message
        self.queue._set_task_state(self.id, state, err=err, message=message)
          
#-------------------------------------------------------------------------
#- The primary Queue class.  Specific backends subclass this and implement
#- the _add(), _get(), etc. methods to provide the functionality

class Queue(object):
    """Definition of Queue interface to be implemented by various backends"""
    
    #- Enumeration of various states a Queue could be in
    ACTIVE  = 'Active'
    PAUSED  = 'Paused'
    UNKNOWN = 'Unknown'
    VALID_STATES = [ACTIVE, PAUSED]
    
    def __init__(self):
        """
        Queue object for managing tasks independent of batch jobs.
        
        Users should get queue objects via the .create() and .connect()
        methods of qdo or specific Backend objects rather than directly
        create them.
        """
        raise NotImplementedError

    def __str__(self):
        count = self.status()['ntasks']
        result = '{} is {}'.format(self.name, self.state)
        for state in Task.VALID_STATES:
            result += '\n{:10s} : {}'.format(state, count[state])
        return result
        
    def __repr__(self):
        return "<qdo.Queue {} at {}>".format(self.name, hex(id(self)))

    def loadfile(self, filename, priority=None):
        """
        Load tasks from a file, one per line
        
        Blank lines and lines starting with # comments are ignored.
        Trailing # comment strings are passed through as part of the
        task string.
        
        if filename == '-' then read from stdin instead.
        
        Optional priority [float] is applied to all tasks in this file.
        """
        tasks = list()
        if filename == '-':
            f = sys.stdin
        else:
            f = open(filename)
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            elif line.startswith('#'):
                continue
            else:
                tasks.append(line)

        priorities = None
        if priority is not None:
            priorities = [priority] * len(tasks)

        return self.add_multiple(tasks, priorities=priorities)
                
    def add(self, task, id=None, priority=None, requires=None):
        """
        Add a single task to the queue
        
        task : any string or object that can be converted into a string
               using json.dumps()

        Optional inputs:
            id       : unique id; if None, then uuid4() will be used
            priority : float.  Higher priorities run first.
            requires : list of task ids that must finish before this runs
            
        Notes on requires:
        Dependencies must finish but they don't have to succeed.
            Failing is still finishing.
        
        Returns taskid added
        """
        return self._add(task, id=id, priority=priority, requires=requires)

    def add_multiple(self, tasks, ids=None, priorities=None, requires=None):
        """
        Add multiple tasks to a queue; see add()
    
        Inputs:
            tasks : array of tasks to add
        
        Optional inputs:
            ids   : array of same length as tasks[] of unique ids
            priorities : array of priorities; higher priorities run first
            requires : array of tuples of tasks upon which this entry
                       depends.  Use None or empty tuple for entries
                       without dependencies.
                       
        Returns list of taskids added.
        """
        return self._add_multiple(tasks, ids=ids, priorities=priorities, requires=requires)

    def set_task_state(self, taskid, state, err=None, message=None):
        """
        Update task to given state (Task.SUCCEDED, Task.FAILED, ...)
        
        Optionally log integer error code and message string.
        """
        self._set_task_state(taskid, state, err=err, message=message)

    def get(self, timeout=60):
        """
        Get a task from the queue and flag as running.
        
        Returns Task object
        
        If no tasks are available within timeout seconds
        or queue is paused, return None
        """
        return self._get(timeout=timeout)
        
    pop = get
    """pop() is the same as get()"""

    def retry(self, state=Task.FAILED):
        """
        Move tasks in `state` (e.g. Task.FAILED) back into pending

        Returns list of taskIDs moved back to pending
        """        
        #- WARNING: depending upon the backend implementation, this could
        #- leave queue/db in an inconsistent state if it fails in the middle.
        return self._retry(state=state)

    def rerun(self):
        """
        Move all completed tasks back into pending (succeeded AND failed)
        
        Returns list of taskIDs moved back to pending
        """
        #- WARNING: depending upon the backend implementation, this could
        #- leave queue/db in an inconsistent state if it fails in the middle.
        return self._rerun()

    def recover(self):
        """
        Recover from mid-task batch job failure by moving running tasks
        back into pending.

        Returns list of taskIDs moved back to pending
        """
        return self.retry(state=Task.RUNNING)

    def do(self, timeout=60, quiet=False, runtime=None, script=None, func=None):
        """
        Process tasks from queue until no tasks are available for
        timeout seconds.
        
        If runtime is given, only process tasks for runtime seconds.

        If *func* is given, pass task to this python fuction.
        
        If *script* is given, run that script with the queued task
        as argument.

        quiet=True will be less chatty.
        
        Returns dictionary with task_count, time elapsed getting tasks,
        and time elapsed running tasks
        """
        n = 0
        time_get = 0.0
        time_run = 0.0
        tstart = time.time()
        while (runtime is None) or (time.time()-tstart < runtime):
            t = time.time()
            task = self.get(timeout=timeout)
            time_get += time.time() - t
            if task is None:
                break  #- no task means we are finished
            else:
                t = time.time()
                try:
                    err = task.run(quiet=quiet, script=script, func=func)
                except KeyboardInterrupt, e:
                    task.set_state(Task.FAILED, err=130, message=repr(e))
                    print "Keyboard Interrupt -- stopping"
                    print self
                    sys.exit(130)
                except Exception, e:
                    task.set_state(Task.FAILED, err=99, message=repr(e))

                time_run += time.time() - t
                n += 1
        
        return {'task_count': n, 'elapsed_get': time_get, 'elapsed_run': time_run}
        # return n

    # run_tasks = do
    # ''' run_tasks() is an alias for do(). '''

    def status(self):
        """
        Return dictionary r describing the queue status
        
        r['state'] = queue state 'Active' or 'Paused'
        r['user'] = user who owns this queue
        r['name'] = name of the queue
        r['ntasks'] = dictionary d[state] = ntasks
            where state = Waiting, Pending, Running, Succeeded, or Failed
            and ntasks = number of tasks in that state        
        """
        results = dict(state=self.state, name=self.name, user=self.user)
        results['ntasks'] = self.tasks(summary=True)
        return results

    summary = status
    """summary() is a synonym for status()"""

    def print_task_state(self):
        """
        DEPRECATED: Print how many jobs are pending/running/succeeded/failed
        
        Printing the queue object itself will do the same thing
        """
        print self

    #- TODO: is this too much feature overloading based upon options?
    #- Maybe q.tasks(summary=True) should be q.ntasks()
    def tasks(self, id=None, state=None, summary=False):
        """
        Return list of Task objects for this queue.

        if state != None, return only tasks in that state
            (see Tasks.VALID_STATES)

        if id != None, return just that Task object.
        raises ValueError if that id doesn't exist in the queue.

        if summary=True, return dictionary of how many tasks are in each state.
        """
        return self._tasks(id=id, state=state, summary=summary)

    def print_tasks(self, verbose=False, state=None):
        """
        Print tasks and their state, including succeeded/failed tasks
        
        if verbose=True, also print ID and error codes
        """
        if verbose:
            print 'State       Err  ID                                Task'
        else:
            print 'State       Task'

        for x in self.tasks(state=state):
            if verbose:
                if x.err is None:
                    print '%-10s  %3s  %s  %s' % (x.state, '---', x.id, x.task)
                else:
                    print '%-10s  %3s  %s  %s' % (x.state, x.err, x.id, x.task)
            else:
                print '%-10s  %s' % (x.state, x.task)

    def delete(self):
        """
        Delete the backend queue
        """
        return self._delete()

    @property
    def state(self):
        """
        Return string describing queue state: Queue.ACTIVE or Queue.PAUSED
        """
        return self._state()

    def pause(self):
        """
        Pause queue to prevent further processing, but allow running tasks
        to cleanly finish.  New tasks won't be processed until resume() is
        called and batch jobs are resubmitted.
        """
        return self._pause()

    def resume(self):
        """
        Restart a paused queue; see Queue.pause()
        """
        return self._resume()

    def launch(self, nworkers, script=None, pack=False, timeout=None,
                    batchqueue=None, walltime=None, batch_opts=''):
        """
        Submit jobs to process tasks from queue

        nworkers    : number of workers to run
        script      : treat tasks in queue as arguments for this script
        pack        : if True, pack multiple workers into a single batch job
        timeout     : timeout for qdo task polling [seconds]
        batchqueue  : batch queue to submit jobs to (diff from qdo queue name)
        walltime    : batch job walltime limit [hh:mm:ss]
        batch_opts  : extra option string to pass to job launcher
        
        $QDO_DIR/etc/batch_config.ini controls the batch configuration options
        """
        if self.state != Queue.ACTIVE:
            print >> sys.stderr, "ERROR: Queue %s is not active" % self.name
            print >> sys.stderr, 'Run "qdo resume %s" first' % self.name
            return
            
        config = _get_batch_config()

        #- Eventually this boils down to
        #- $QDO_MPIRUN $QDO_DIR/bin/qdo do $QDO_NAME $QDO_OPTS

        #- Make sure we know where to find qdo
        if 'QDO_DIR' not in os.environ:
            os.environ['QDO_DIR'] = _qdo_dir()

        #- Set name of qdo queue
        os.environ['QDO_NAME'] = self.name

        #- Construct options to pass to "qdo do" via $QDO_OPTS
        qdo_opts = list()
        if script is not None:
            qdo_opts.extend(["--script", script])
        elif 'QDO_SCRIPT' in os.environ:
            qdo_opts.extend(["--script", os.environ['QDO_SCRIPT']])

        if timeout is not None:
            qdo_opts.extend(["--timeout", str(timeout)])
        elif 'QDO_TIMEOUT' in os.environ:
            qdo_opts.extend(["--timeout", str(os.environ['QDO_TIMEOUT'])])

        if pack:
            qdo_opts.extend(["--jitter", "{:.2f}".format(nworkers/4.0)])

        os.environ['QDO_OPTS'] = ' '.join(qdo_opts)

        #- Construct options to pass to job launcher
        if batch_opts is None:
            batch_opts = ''
        if walltime is not None:
            batch_opts += ' ' + config['walltime_opt'].format(walltime=walltime)

        if pack:
            workers_per_node = config['cores_per_node']
            if nworkers % workers_per_node != 0:
                print >> sys.stderr, "ERROR: nworkers={} must be evenly divisible by {}".format(nworkers, workers_per_node)
                return

            nodes = nworkers // workers_per_node
            os.environ['QDO_WORKERS_PER_JOB'] = str(nworkers)

            if batchqueue is None and config.has_key('parallel_queue'):
                batchqueue = config['parallel_queue']                                
        else:
            workers_per_node = 1
            nodes = nworkers
            os.environ['QDO_WORKERS_PER_JOB'] = '1'
            if batchqueue is None and config.has_key('serial_queue'):
                batchqueue = config['serial_queue']

        if batchqueue is not None:
            batch_opts += ' '+config['batchqueue_opt'].format(batchqueue=batchqueue)

        #- Options for constructing qsub command
        opts = dict(nworkers=nworkers, nodes=nodes,
            workers_per_node=workers_per_node,
            qdo_dir = os.getenv('QDO_DIR'),
            jobname=self.name,
            batch_opts=batch_opts )

        #- How should MPI be run?  Don't use MPI at all in serial queue
        if config.has_key('serial_queue') and (batchqueue == config['serial_queue']):
            os.environ['QDO_MPIRUN'] = ''
        else:
            os.environ['QDO_MPIRUN'] = config['mpirun'].format(nworkers=nworkers)

        #- Finally ready to submit jobs!
        if pack:
            cmd = config['qsub'].format(**opts)
            print cmd
            os.system(cmd)
        else:
            opts['nodes'] = 1
            for i in range(nworkers):
                cmd = config['qsub'].format(**opts)
                print cmd
                os.system(cmd)

    #---------------------------------------------------------------------
    #- Backends should implement these methods; see the doc strings for
    #- the non-underscore versions of these functions for parameters.

    def _add(self, task, id=None, priority=None, requires=None):
        raise NotImplementedError
    
    #- Backend could override with more efficient implementation
    def _add_multiple(self, tasks, ids=None, priorities=None, requires=None):
        results = list()
        if ids is None:
            ids = [None,] * len(tasks)
        if priorities is None:
            priorities = [None,] * len(tasks)
        if requires is None:
            requires = [None,] * len(tasks)
            
        for i, task in enumerate(tasks):
            results.append(self.add(task, id=ids[i], priority=priorities[i], requires=requires[i]))
        
        return results
    
    def _get(self, timeout):
        raise NotImplementedError 
         
    def _set_task_state(self, taskid, state, err=None, message=None):
        raise NotImplementedError

    def _retry(self):
        raise NotImplementedError 

    def _rerun(self):
        raise NotImplementedError 
     
    def _pause(self):
        raise NotImplementedError 
     
    def _resume(self):
        raise NotImplementedError 
     
    def _delete(self):
        raise NotImplementedError 

    def _state(self):
        raise NotImplementedError 

    def _tasks(self, id=None, state=None, summary=False):
        raise NotImplementedError 

#-------------------------------------------------------------------------
#- Backends

#- Backends modules should implement:
#-   a subclass of qdo.Backend (see below)
#-   a subclass of qdo.Queue (see above)

#- qdo.Backend subclasses should implement __init__() with backend specific
#- parameters for how to connect to queues with this backend
#- (e.g. a directory for sqlite or a URL and token for a REST API).

#- Backend subclasses should also implement create(), connect(), and qlist(),
#- but do not alter the call signature for these.  Once a user has a
#- Backend object, the create/connect/qlist usage should be identical
#- across all backends.

class Backend(object):
    """Base class for qdo backends"""
    def __init__(self):
        """
        Returns a new Backend object
        
        Specific backends will add other input parameters as needed
        """
        raise NotImplementedError("Create a specific backend subclass, not the top level qdo.Backend")
        
    def create(self, queuename, user=None, worker=None):
        """
        Create a new queue and return a subclass of qdo.Queue
        
        Raises ValueError if queue_name already exists
        
        Optional inputs:
            user    : name of user who owns this queue (default $USER)
            worker  : name of worker connecting to this queue
        """
        raise NotImplementedError
        
    def connect(self, queuename, user=None, worker=None, create_ok=False):
        """
        Return a subclass of qdo.Queue representing queuename
        
        Optional inputs:
            user      : name of user who owns this queue (default $USER)
            worker    : name of worker connecting to this queue
            create_ok : if True, create queue if needed.  If False (default),
                        raise ValueError if queue doesn't already exist.
        """
        raise NotImplementedError
        
    def qlist(self, user=None, worker=None):
        """
        Return a list of qdo.Queue objects for known queues

        Optional inputs:
            user    : name of user who owns this queue (default $USER)
            worker  : name of worker connecting to these queues
        """
        raise NotImplementedError
        
    
#- Default backend is sqlite
import qdo.backends
_backend = qdo.backends.SqliteBackend()

#- Add qdo module level create(), connect(), and qlist() functions that
#- use the default backend if the user is happy with that choice.

def create(queue_name, user=None, worker=None):
    """Connect to a pre-existing queue or create one if needed"""
    return _backend.create(queue_name, user=user, worker=worker)

def connect(queue_name, user=None, worker=None, create_ok=False):
    """Connect to a pre-existing Queue"""
    return _backend.connect(queue_name, user=user, worker=worker, create_ok=create_ok)

def qlist(user=None):
    """Return list of Queue objects for this user"""
    return _backend.qlist(user)

#-------------------------------------------------------------------------
#- Convenience and helper functions

def print_queues(user=None):
    """Print all active queues"""

    queues = _backend.qlist(user)
    if len(queues) > 0:
        if user is None:
            print "QueueName                  User   State   Waiting  Pending  Running Succeeded   Failed"
        else:
            print "QueueName              State   Waiting  Pending  Running Succeeded   Failed"
        for q in queues:
            cmd_count = q.status()['ntasks']
            if user is None:          
                print "%-20s %10s %8s" % (q.name, q.user, q.state),
            else:
                print "%-20s %8s" % (q.name, q.state),
                
            print "%8d %8d %8d  %8d %8d" % (cmd_count[Task.WAITING],
                                            cmd_count[Task.PENDING],
                                            cmd_count[Task.RUNNING],
                                            cmd_count[Task.SUCCEEDED],
                                            cmd_count[Task.FAILED] )
    else:
        print "No known QDO queues"

def valid_queue_name(name):
    """
    Return True/False if name is a valid Queue name.

    [A-Za-z9-0_-] but no double-underscores allowed
    """
    ok = True
    if name.count('__') > 0:
        print "Sorry, double underscores are not allowed in queue names"
        ok = False
    # if name.count('-') > 0:
    #     print "Sorry, hyphens are not allowed in queue names"
    #     ok = False
    elif not name.replace('_', '').replace('-', '').isalnum():
        print "Sorry, queue names must be alphanumeric plus individual underscores and hyphens"
        ok = False
    
    return ok

def _qdo_dir():
    """Return $QDO_DIR, or if not set return location of qdo intallation"""
    qd = os.environ.get('QDO_DIR', None)
    if qd is None:
        # parse /path/to/qdo/py/qdo/__init__.py -> /path/to/qdo
        qd = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

    return qd

def _get_batch_config():
    """
    Read batch_config.ini file and return dictionary of parameters to use
    """
    cp = SafeConfigParser()
    cp.read(os.path.join(_qdo_dir(), 'etc', 'batch_config.ini'))
    profile = os.getenv('QDO_BATCH_PROFILE', None)
    if profile is None:
        profile = cp.get('default', 'profile')
                
    config = dict(cp.items(profile))
    
    #- If cores_per_nodes isn't specified, guess based upon submission host
    if 'cores_per_node' not in config:
        from multiprocessing import cpu_count
        config['cores_per_node'] = cpu_count()
    else:
        config['cores_per_node'] = int(config['cores_per_node'])
    
    return config

#- Utility function to define default worker name
def _worker():
    import socket
    return socket.gethostname() + '-' + str(os.getpid())
        


