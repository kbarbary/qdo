"""
Installer for QDO
"""
import os
import re
import sys
import ez_setup
ez_setup.use_setuptools()
from setuptools import setup, find_packages
# added command classes

# Do the setup
setup(
    name="qdo",
    packages=find_packages(),
    version="0.5",
    # install_requires=[s.strip() for s in open("requirements.txt")],
    extras_require={},
    # package_data={"": ["*.json"]},
    author="Stephen Bailey",
    author_email="StephenBailey@lbl.gov",
    maintainer="Stephen Bailey",
    url="https://bitbucket.org/berkeleylab/qdo",
    license="BSD 3-clause",
    description="QDO",
    long_description="QDO (kew-doo) is a lightweight high-throughput queuing system designed for workflows that have many independent tasks to perform.",
    keywords=["qdo", "workflow"],
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Software Development :: Libraries :: Python Modules"
    ],
    ext_modules=[],
    #cmdclass = {
    #    "doc": BuildDocumentation,
    #},
)
