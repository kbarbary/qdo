## Priorities ##

The qdo python interface includes the ability to set the priority of
submitted tasks.  The default priority is 0.  Tasks with numerically higher
priority will be run first.  Priorities can also be set negative to give
them lower than default priority.

Example to give higher priority to every 5th task submitted:

    q = qdo.create('EchoChamber1')
    for i in range(20):
        if i%5 == 0:
            q.add('echo '+str(i), priority=10)
        else:
            q.add('echo '+str(i))  #- default priority is 0
        
    q.do(timeout=1, quiet=True)
    
Results:

    15
    10
    5
    0
    19
    18
    17
    ...
    3
    2
    1
    
Priorities can also be set with add_multiple:

    q = qdo.create('EchoChamber1')
    n = 20
    commands = ['echo '+str(i) for i in range(n)]
    priorities = [i%5 == 0 for i in range(n)]
    q.add_multiple(commands, priorities=priorities)
    q.do(timeout=1, quiet=True)

Notes:

  * Tasks with the same priority could run in any order.  In this case
    the highest priority jobs happened to run in reverse order of submission,
    but that is arbitrary.
  * Task dependencies trump priorities: A high priority task will not start
    until all of its dependencies have been met, possibly allowing lower
    priority tasks to start first if all of their dependencies are met first.  
  * Even after jobs have started running to process tasks, you can submit
    a new task with a high priority to have it jump to the front of the queue
    to be run in the next available slot.
    
Not yet implemented:

  * adjusting priorities after tasks have been submitted
    

    